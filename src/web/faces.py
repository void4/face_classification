from flask import Flask, jsonify, make_response, request, abort, redirect, send_file
import logging
from time import time
import emotion_gender_processor as eg_processor
import sys

#print("STARTING", file=sys.stderr)
app = Flask(__name__)

@app.route('/')
def index():
	#return redirect("https://ekholabs.ai", code=302)
    return make_response(jsonify({'error': 'Make a POST request to /classifyImage with the image=@pathtoimage\nFor example:\ncurl -v -F image=@path/to/image.png http://localhost:4000/classifyImage > image.png'}), 400)

@app.route('/classifyImage', methods=['POST'])
def image():
	try:
		image = request.files['image'].read()
		#start = time()
		emotions = eg_processor.process_image(image)
		#app.logger.info("Took %f to classify image" % (time()-start))
		return send_file('/ekholabs/face-classifier/result/predicted_image.png', mimetype='image/png')
	except Exception as err:
		logging.error('An error has occurred whilst processing the file: "{0}"'.format(err))
		abort(400)

@app.route('/classifyText', methods=['POST'])
def text():
	try:
		image = request.files['image'].read()
		#start = time()
		emotions = eg_processor.process_image(image, annotate=False)
		#app.logger.info("Took %f to classify image" % (time()-start))
		return jsonify(emotions)
	except Exception as err:
		logging.error('An error has occurred whilst processing the file: "{0}"'.format(err))
		abort(400)

@app.errorhandler(400)
def bad_request(erro):
	return make_response(jsonify({'error': 'We cannot process the file sent in the request.'}), 400)

@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error': 'Resource no found.'}), 404)

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=8084)
